from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

bot = webdriver.Chrome(r"C:/Users/HP/Desktop/Transviti/Protest/chromedriver.exe")
url="http://www.saudisale.com/SS_e_mpg.aspx"

bot.get(url)

WebDriverWait(bot,20).until(EC.presence_of_element_located((By.XPATH,'//*[@id="GridView1_ctl02_Label10"]')))
make = bot.find_element(By.XPATH, '//*[@id="GridView1_ctl02_Label10"]')
print('Make: ', make.text)

WebDriverWait(bot,20).until(EC.presence_of_element_located((By.XPATH,'//*[@id="GridView1_ctl02_Label3"]')))
class_ = bot.find_element(By.XPATH, '//*[@id="GridView1_ctl02_Label3"]')
print('Class: ', class_.text)

WebDriverWait(bot,20).until(EC.presence_of_element_located((By.XPATH,'//*[@id="GridView1_ctl02_Label13"]')))
model = bot.find_element(By.XPATH, '//*[@id="GridView1_ctl02_Label13"]')
print('Model: ', model.text)

WebDriverWait(bot,20).until(EC.presence_of_element_located((By.XPATH,'//*[@id="GridView1_ctl02_Label6"]')))
year = bot.find_element(By.XPATH, '//*[@id="GridView1_ctl02_Label6"]')
print('Year: ', year.text)