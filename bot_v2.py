from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import xlsxwriter
import time

workbook = xlsxwriter.Workbook('data.xlsx')
worksheet = workbook.add_worksheet()

excel_row_counter = 0

worksheet.write(excel_row_counter, 0, 'Make')
worksheet.write(excel_row_counter, 1, 'Class')
worksheet.write(excel_row_counter, 2, 'Model')
worksheet.write(excel_row_counter, 3, 'Year')

excel_row_counter += 1

bot = webdriver.Chrome(r"C:/Users/HP/Desktop/Transviti/Protest/chromedriver.exe")
url="http://www.saudisale.com/SS_e_mpg.aspx"

bot.get(url)

page_counter = 1

for ten_pages in range(42):
	if ten_pages == 0:
		page_start_range = 0
	else:
		page_start_range = 2

	for page in range(page_start_range, page_start_range + 10):
		while True:
			try:
				WebDriverWait(bot,1).until(EC.presence_of_element_located((By.XPATH,f'//*[@id="GridView1"]/tbody/tr[43]/td/table/tbody/tr/td[{page+1}]/a')))
				print('page loading')
				time.sleep(1)
			except:
				break
		
		print(f'Page {page_counter}')
		page_counter += 1

		for i in range(8):
			WebDriverWait(bot,20).until(EC.presence_of_element_located((By.XPATH,f'//*[@id="GridView1_ctl0{i+2}_Label10"]')))
			make = bot.find_element(By.XPATH, f'//*[@id="GridView1_ctl0{i+2}_Label10"]').text

			WebDriverWait(bot,20).until(EC.presence_of_element_located((By.XPATH,f'//*[@id="GridView1_ctl0{i+2}_Label3"]')))
			class_ = bot.find_element(By.XPATH, f'//*[@id="GridView1_ctl0{i+2}_Label3"]').text

			WebDriverWait(bot,20).until(EC.presence_of_element_located((By.XPATH,f'//*[@id="GridView1_ctl0{i+2}_Label13"]')))
			model = bot.find_element(By.XPATH, f'//*[@id="GridView1_ctl0{i+2}_Label13"]').text

			WebDriverWait(bot,20).until(EC.presence_of_element_located((By.XPATH,f'//*[@id="GridView1_ctl0{i+2}_Label6"]')))
			year = int(bot.find_element(By.XPATH, f'//*[@id="GridView1_ctl0{i+2}_Label6"]').text)

			data = [make, class_, model, year]
			for j in range(len(data)):
				worksheet.write(excel_row_counter, j, data[j])
			excel_row_counter += 1


		for i in range(9, 43):
			WebDriverWait(bot,20).until(EC.presence_of_element_located((By.XPATH,f'//*[@id="GridView1_ctl{i+1}_Label10"]')))
			make = bot.find_element(By.XPATH, f'//*[@id="GridView1_ctl{i+1}_Label10"]').text

			WebDriverWait(bot,20).until(EC.presence_of_element_located((By.XPATH,f'//*[@id="GridView1_ctl{i+1}_Label3"]')))
			class_ = bot.find_element(By.XPATH, f'//*[@id="GridView1_ctl{i+1}_Label3"]').text

			WebDriverWait(bot,20).until(EC.presence_of_element_located((By.XPATH,f'//*[@id="GridView1_ctl{i+1}_Label13"]')))
			model = bot.find_element(By.XPATH, f'//*[@id="GridView1_ctl{i+1}_Label13"]').text

			WebDriverWait(bot,20).until(EC.presence_of_element_located((By.XPATH,f'//*[@id="GridView1_ctl{i+1}_Label6"]')))
			year = int(bot.find_element(By.XPATH, f'//*[@id="GridView1_ctl{i+1}_Label6"]').text)

			data = [make, class_, model, year]
			for j in range(len(data)):
				worksheet.write(excel_row_counter, j, data[j])
			excel_row_counter += 1

		WebDriverWait(bot,20).until(EC.presence_of_element_located((By.XPATH,f'//*[@id="GridView1"]/tbody/tr[43]/td/table/tbody/tr/td[{page+2}]')))
		bot.find_element(By.XPATH, f'//*[@id="GridView1"]/tbody/tr[43]/td/table/tbody/tr/td[{page+2}]').click()

	WebDriverWait(bot,20).until(EC.presence_of_element_located((By.XPATH,f'//*[@id="GridView1"]/tbody/tr[43]/td/table/tbody/tr/td[{page_start_range + 11}]')))
	bot.find_element(By.XPATH, f'//*[@id="GridView1"]/tbody/tr[43]/td/table/tbody/tr/td[{page_start_range + 11}]').click()

workbook.close()